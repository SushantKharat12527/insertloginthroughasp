﻿using Sol_InsertDataIntoDatabaseAndLoginCheck.Dal;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity.PersonEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_InsertDataIntoDatabaseAndLoginCheck
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUserName != null&&txtPassword.MaxLength>=8)
            {
                // Mapping
                var userEntityObj = this.MapLoginData();

                // Submit Registration
                var result = new PersonDal().RegistrationAsync(userEntityObj);
            }

        }

        private PersonEntity MapLoginData()
        {
           
                try
                {

                    PersonEntity userEntityObj = new PersonEntity()

                    {


                        loginEntity = new LoginEntity()
                        {
                            UserName = txtUserName.Text,
                            Password = txtPassword.Text
                        }

                    };

                    return userEntityObj;



                }
                catch (Exception)
                {
                    throw;
                }
            }
            
        }

        protected void btnRegistration_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RegistrationPage.aspx", false);
        }

    }
}