﻿using Sol_InsertDataIntoDatabaseAndLoginCheck.Dal.ORD;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity.PersonEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_InsertDataIntoDatabaseAndLoginCheck.Dal
{
    public class PersonDal
    {
        #region Declaration
        private PersonLoginCheckDataContext _dbLoginCheck = null;

        private PersonRegistrationDataContext _dbRegistration = null;
        #endregion

        #region Constructor
        public PersonDal()
        {
            _dbLoginCheck = new PersonLoginCheckDataContext();
            _dbRegistration = new PersonRegistrationDataContext();
        }

        #endregion

        #region Property

        #endregion

        #region Public Method
        public dynamic CheckLoginAsync(PersonEntity personEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
               var getQuery= _dbLoginCheck
               ?.uspLoginCheck(
                   "CheckLogin"
                   , personEntityObj?.loginEntity?.UserName
                   , personEntityObj?.loginEntity?.Password
                   , ref status
                   , ref message
                   );
                return message;
            }
            catch (Exception)
            {
                throw;
            }

        }


        public dynamic RegistrationAsync(PersonEntity personEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
               
                  var getQuery=  _dbRegistration
                         ?.uspSetCustomer(
                             "Add"
                             , personEntityObj?.personId
                             , personEntityObj?.FirstName
                             , personEntityObj?.LastName
                             , personEntityObj?.communicationEntity?.MobileNo
                             , personEntityObj?.communicationEntity?.EmailId
                             , personEntityObj?.loginEntity?.UserName
                             , personEntityObj?.loginEntity?.Password
                             , ref status
                             , ref message

                             );

                    return getQuery;
               
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}