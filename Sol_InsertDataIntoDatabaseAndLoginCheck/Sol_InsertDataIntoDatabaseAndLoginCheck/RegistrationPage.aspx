﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="RegistrationPage.aspx.cs" Inherits="Sol_InsertDataIntoDatabaseAndLoginCheck.RegistrationPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table>
         <tr>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" placeHolder="FirstName"></asp:TextBox>
                        </td>
                    </tr>
         <tr>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" placeHolder="LastName"></asp:TextBox>
                        </td>
                    </tr>
          <tr>
                        <td>
                            <asp:TextBox ID="txtEmailId" runat="server" placeHolder="Email"></asp:TextBox>
                        </td>
                    </tr>
          <tr>
                        <td>
                            <asp:TextBox ID="txtMobileNo" runat="server" placeHolder="Mobile No "></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" placeHolder="User Name" ></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" placeHolder="Password" TextMode="Password" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    
                        <td>
                            <asp:Button ID="btnRegister" runat="server" Text="Register" OnClick="btnRegister_Click" />
                        </td>
                    </tr>
                </table>
    </div>
    </form>
</body>
</html>
