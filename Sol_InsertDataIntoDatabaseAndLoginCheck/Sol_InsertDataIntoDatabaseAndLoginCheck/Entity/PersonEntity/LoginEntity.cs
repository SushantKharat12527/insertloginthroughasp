﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_InsertDataIntoDatabaseAndLoginCheck.Entity
{
    public class LoginEntity
    {
        public String UserName { get; set; }

        public String Password { get; set; }
    }
}