﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_InsertDataIntoDatabaseAndLoginCheck.Entity.PersonEntity
{
    public class PersonEntity
    {
        public decimal? personId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PersonType { get; set; }
        public CommunicationEntity communicationEntity { get; set; }
        public LoginEntity loginEntity { get; set; }


    }
}