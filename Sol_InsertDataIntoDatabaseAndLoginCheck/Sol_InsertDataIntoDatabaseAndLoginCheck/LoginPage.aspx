﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="Sol_InsertDataIntoDatabaseAndLoginCheck.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:ScriptManager ID="ScriptManager" runat="server">
            
           
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                  <ContentTemplate>

    <table>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" placeHolder="User Name"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" placeHolder="Password" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           
                            <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnRegistration" runat="server" Text="Register" OnClick="btnRegistration_Click" />
                        </td>
                    </tr>
                </table>
                       </ContentTemplate>
            </asp:UpdatePanel>
</asp:ScriptManager>
    </div>
    </form>
</body>
</html>
