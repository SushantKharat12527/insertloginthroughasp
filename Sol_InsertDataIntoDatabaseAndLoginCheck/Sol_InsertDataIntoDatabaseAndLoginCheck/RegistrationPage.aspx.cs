﻿using Sol_InsertDataIntoDatabaseAndLoginCheck.Dal;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity.PersonEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_InsertDataIntoDatabaseAndLoginCheck
{
    public partial class RegistrationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                // Mapping
                var userEntityObj = this.MapPersonData();

                // Submit Registration
                var result = new PersonDal().RegistrationAsync(userEntityObj);

            
              
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        private PersonEntity MapPersonData()
        {
            try
            {
               
                PersonEntity userEntityObj = new PersonEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,

                    communicationEntity = new CommunicationEntity()
                    {
                        MobileNo = txtMobileNo.Text,
                        EmailId = txtEmailId.Text
                    },

                    loginEntity = new LoginEntity()
                    {
                        UserName = txtUserName.Text,
                        Password = txtPassword.Text
                    }

                };

                return userEntityObj;
                


            }
            catch (Exception)
            {
                throw;
            }
        }

     
    }
}