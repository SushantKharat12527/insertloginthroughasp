﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity.PersonEntity;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Entity;
using Sol_InsertDataIntoDatabaseAndLoginCheck.Dal;

namespace UTP
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LoginCheckTestMethod()
        {
            var personEntityObj = new PersonEntity()
            {
                loginEntity = new LoginEntity()
                {
                    UserName = "Sonu123",
                    Password = "Nigam123"

                }
            };
            var result = new PersonDal().CheckLoginAsync(personEntityObj);

            Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void RegistrationTestMethod()
        //{
        //    var personEntityObj = new PersonEntity()
        //    {
        //        FirstName = "Shreya",
        //        LastName = "Ghoshal",
        //        communicationEntity = new CommunicationEntity()
        //        {
        //            MobileNo = "3456789124",
        //            EmailId = "sword555@gmail.com"
        //        },
        //        loginEntity = new LoginEntity()
        //        {
        //            UserName = "Sonu123",
        //            Password = "Nigam123"

        //        }
        //    };

        //    var result = new PersonDal().RegistrationAsync(personEntityObj);
        //    Assert.IsNotNull(result);
        //}
    }
}
